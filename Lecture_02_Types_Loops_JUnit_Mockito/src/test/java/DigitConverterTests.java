import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.DigitConverter;
import ru.edu.lesson1.DigitConverterHard;
import ru.edu.lesson1.DigitConverterImpl;

public class DigitConverterTests {

    private DigitConverter converter = new DigitConverterImpl();

    private DigitConverterHard converterHard = new DigitConverterImpl();

    @Test
    public void intConverterTest() {

        Assert.assertNotNull(converter);

        Assert.assertEquals("11111100101", converter.convert(2021, 2));
        Assert.assertEquals("2202212", converter.convert(2021, 3));
        Assert.assertEquals("133211", converter.convert(2021, 4));
        Assert.assertEquals("31041", converter.convert(2021, 5));
        Assert.assertEquals("13205", converter.convert(2021, 6));
        Assert.assertEquals("5615", converter.convert(2021, 7));
        Assert.assertEquals("3745", converter.convert(2021, 8));
        Assert.assertEquals("2685", converter.convert(2021, 9));
        Assert.assertEquals("2021", converter.convert(2021, 10));
        Assert.assertEquals("0", converter.convert(0, 2));
        Assert.assertEquals("1111", converter.convert(4, 1));

    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownRadix() {
        converter.convert(2021, 0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownDigit() {
        converter.convert(-1, 2);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownBoth() {
        converter.convert(-1, 0);
    }

    /**
     * Закомментировать @Test если не делаете задачу со звездочкой
     */
    @Test
    public void doubleConverterTest() {

        Assert.assertNotNull(converterHard);

        Assert.assertEquals("0.10000", converterHard.convert(0.5, 2, 5));
        Assert.assertEquals("0.01", converterHard.convert(0.25, 2, 2));
        Assert.assertEquals("11111100101.00110", converterHard.convert(2021.2022, 2, 5));
        Assert.assertEquals("2202212.01211", converterHard.convert(2021.2022, 3, 5));
        Assert.assertEquals("133211.03033", converterHard.convert(2021.2022, 4, 5));
        Assert.assertEquals("31041.10011", converterHard.convert(2021.2022, 5, 5));
        Assert.assertEquals("13205.11140", converterHard.convert(2021.2022, 6, 5));
        Assert.assertEquals("5615.12623", converterHard.convert(2021.2022, 7, 5));
        Assert.assertEquals("3745.14741", converterHard.convert(2021.2022, 8, 5));
        Assert.assertEquals("2685.17335", converterHard.convert(2021.2022, 9, 5));
        Assert.assertEquals("2021.20220", converterHard.convert(2021.2022, 10, 5));

    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownRadixHard() {
        converterHard.convert(2021.2022, 0, 5);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownDigitHard() {
        converterHard.convert(-1.2, 2, 5);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionThrownBothHard() {
        converterHard.convert(-1.5, 1, 5);
    }
    /*
     * Добавить тестирование сценариев с некорректным вводом для получения исключения для реализованных методов
     */
}
