package ru.edu.lesson1;

import java.math.BigDecimal;
import java.util.Arrays;

public class DigitConverterImpl implements DigitConverterHard{
    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(int digit, int radix) {
        int tmp = digit;
        String result = "";

        if (digit < 0) {
            throw new IllegalArgumentException("Exception: digit is negative number!");
        }

        if (radix < 1) {
            throw new IllegalArgumentException("Exception: radix < 1!");
        }

        if (digit == 0) {
            return "0";
        }

        if (radix == 1) {
            char[] repeat = new char[digit];
            Arrays.fill(repeat, '1');
            return new String(repeat);
        }

        while (tmp > 0) {
            result = String.valueOf(tmp % radix) + result;
            tmp /= radix;
        }

        return result;
    }

    /**
     * Преобразование вещественного десятичного числа в другую систему счисления
     *
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(double digit, int radix, int precision) {
        BigDecimal doubleDigit = new BigDecimal(String.valueOf(digit));
        int intPart = doubleDigit.intValue();
        BigDecimal fractPart = doubleDigit.subtract(BigDecimal.valueOf(intPart));
        String result = "";

        if (digit < 0) {
            throw new IllegalArgumentException("Exception: digit is negative number!");
        }

        if (radix <= 1) {
            throw new IllegalArgumentException("Exception: radix <= 1!");
        }

        if (digit == 0) {
            return "0";
        }

        if (intPart == 0) {
            result = "0";
        }
        else while (intPart > 0) {
            result = String.valueOf(intPart % radix) + result;
            intPart /= radix;
        }

        result = result + ".";

        for (int i = 0; i < precision; i++) {
            fractPart = fractPart.multiply(BigDecimal.valueOf(radix));
            result = result + String.valueOf(fractPart.intValue());
            fractPart = fractPart.subtract(BigDecimal.valueOf(fractPart.intValue()));
        }

        return result;
    }
}
