package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GreetingImpl implements IGreeting{

    private List<Hobby> hobbies = new ArrayList<>();
    private String firstName = "Петр";
    private String secondName = "Алексеевич";
    private String lastName = "Трошенков";
    private String bitbucketUrl = "https://bitbucket.org/p_troshenkov/";
    private String phone = "+79200280063";
    private String courseExpectation = "get a new profession";
    private String educationInfo = "technical secondary school: CNC machine operator";

    public GreetingImpl() {
        hobbies.add(new Hobby("1", "there's no more time for hobbies"));
        hobbies.add(new Hobby("2", "skiing"));

    }

    /**
     * Get first name.
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get second name
     */
    @Override
    public String getSecondName() {
        return secondName;
    }

    /**
     * Get last name.
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Get hobbies.
     */
    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    /**
     * Get bitbucket url to your repo.
     */
    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    /**
     * Get phone number.
     */
    @Override
    public String getPhone() {
        return phone;
    }

    /**
     * Your expectations about course.
     */
    @Override
    public String getCourseExpectation() {
        return courseExpectation;
    }

    /**
     * Print your university and faculty here.
     */
    @Override
    public String getEducationInfo() {
        return educationInfo;
    }

    @Override
    public String toString() {
        return "GreetingImpl{" +
                "hobbies=" + hobbies +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", bitbucketUrl='" + bitbucketUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", courseExpectation='" + courseExpectation + '\'' +
                ", educationInfo='" + educationInfo + '\'' +
                '}';
    }
}
