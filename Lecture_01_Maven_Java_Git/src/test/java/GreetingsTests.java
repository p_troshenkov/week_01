import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.GreetingImpl;
import ru.edu.lesson1.Hobby;
import ru.edu.lesson1.IGreeting;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class GreetingsTests {

    private IGreeting greeting = new GreetingImpl();




    @Test
    public void size_Test() {

        assertNotNull(greeting);

        assertEquals("Петр", greeting.getFirstName());
        assertEquals("Алексеевич", greeting.getSecondName());
        assertEquals("Трошенков", greeting.getLastName());
        assertEquals("https://bitbucket.org/p_troshenkov/", greeting.getBitbucketUrl());
        assertEquals("+79200280063", greeting.getPhone());
        assertEquals("get a new profession", greeting.getCourseExpectation());
        assertEquals("technical secondary school: CNC machine operator", greeting.getEducationInfo());


        assertEquals(new Hobby("1", "there's no more time for hobbies").toString(),
                ((List)greeting.getHobbies()).get(0).toString());

        assertEquals(new Hobby("2", "skiing").toString(),
                ((List)greeting.getHobbies()).get(1).toString());

        assertFalse(greeting.toString().isEmpty());
        // check your methods
    }
}
